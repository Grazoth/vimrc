"{{{ Configuration de Vundle et de ses plugins
"
" see :h vundle for more details or wiki for FAQ
"
" Toutes les commandes "Plugin 'plugpath' doivent se trouver entre 
" 'call vundle#begin()' et 'call vundle#end()'
"
" The following are examples of different formats supported.
"   Plugin 'tpope/vim-fugitive' " plugin on GitHub repo
"   Plugin 'L9' " plugin from http://vim-scripts.org/vim/scripts.html
"   Plugin 'git://git.wincent.com/command-t.git' " Git not hosted on GitHub
"   Plugin 'file:///home/gmarik/path/to/plugin' " git repos on local machine
"
" On peut changer le nom d’un plugin
"   Plugin 'ascenator/L9', {'name': 'newL9'}
"
" Ou spécifier un runtime path différent
"   Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
"
" }}}


set nocompatible " be iMproved, required
filetype off     " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin() " Keep Plugin commands between vundle#begin/end.

Plugin 'VundleVim/Vundle.vim' " let Vundle manage Vundle, required

" Explorateurs de fichiers {{{
Plugin 'sjbach/lusty.git'
Plugin 'scrooloose/nerdtree.git'
Plugin 'ctrlpvim/ctrlp.vim.git'
" }}}

" Analyseurs de syntaxe {{{
Plugin 'scrooloose/syntastic.git'
Plugin 'dense-analysis/ale' " Remplace pyflakes qui est déprécié
Plugin 'sheerun/vim-polyglot' " Gestion de la syntaxe pour plein de language

" }}}

" {{{ autocomplétion
" Plugin 'Valloric/YouCompleteMe'
Plugin 'Shougo/deoplete.nvim' " Remplace YouCompleteMe

Plugin 'roxma/nvim-yarp' " Requis par deoplete.nvim
Plugin 'roxma/vim-hug-neovim-rpc' " Requis par deoplete.nvim
Plugin 'deoplete-plugins/deoplete-jedi' " plugin complétion python

Plugin 'mattn/emmet-vim'
" }}}
"
" Autre {{{
Plugin 'vim-scripts/indentpython.vim' " indente correctement le code python
Plugin 'Yggdroot/indentLine' " met en surbrillance les niveau d’indentation
Plugin 'tpope/vim-commentary' " Commenter facilement du code

Plugin 'hail2u/vim-css3-syntax' " Meilleure coloration syntaxique pour CSS3
Plugin 'pangloss/vim-javascript'
Plugin 'heavenshell/vim-jsdoc'

Plugin 'stevearc/vim-arduino'
Plugin 'tpope/vim-surround.git'
Plugin 'tpope/vim-fugitive' " Controler git depuis vim

Plugin 'ludovicchabant/vim-gutentags' " Remplace auto craigemery/vim-autotag
Plugin 'vim-airline/vim-airline'

Plugin 'vifm/vifm.vim'
Plugin 'jakykong/vim-zim'

Plugin 'vim-scripts/po.vim'
" }}}

" ColorSheme {{{
Plugin 'jnurmine/Zenburn'
Plugin 'dikiaap/minimalist'
Plugin 'badwolf'
" }}}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Put your non-Plugin stuff after this line
" vim: foldmethod=marker
